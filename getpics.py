# usage : python filename.py link_to_thread.html /path/to/folder
# path to folder is optional

#script tested with 4chan and 2ch.hk

from HTMLParser import HTMLParser
import urllib
import sys
import os
import time
import wget
import httplib

def main():
    reload(sys)
    sys.setdefaultencoding('utf-8')

    links = []
    class MyHTMLParser(HTMLParser):
        def handle_starttag(self, tag, attrs):
            if tag == "a":
                for attr in attrs:
                    print attrs
                    if attr[0] == 'href' and (attr[1].endswith('webm') or attr[1].endswith('gif') or attr[1].endswith('png') or attr[1].endswith('jpeg') or attr[1].endswith('jpg')) and ('src' in attr[1] or 'i' in attr[1]):
                        links.append(attr[1])

    page_link = sys.argv[1]
    protocol = page_link.split("://")[0]
    chan = page_link.split("/")[2] # [1] is empty, caused by // after protocol
    board = page_link.split("/")[3]
    print "page:\t" + page_link
    print "protocol:\t" + protocol
    print "chan:\t" + chan
    print "board:\t" + board

    sock = urllib.urlopen(page_link)
    htmlSource = sock.read()
    #print htmlSource
    parser = MyHTMLParser()
    parser.feed(htmlSource)
    sock=urllib.urlopen(page_link)
    htmlSource = sock.read()
    sock.close()
    parser.feed(htmlSource)

    folder = 'content' + time.strftime("%H%M%S%d%m%Y")
    if len(sys.argv) >= 3:
        folder = sys.argv[2]

    if not os.path.exists(folder):
        os.mkdir(folder)
    os.chdir(folder)
    print "destination:\t" + folder

    thread = (sys.argv[1].split("/")[-1]).split(".")[0]
    print "thread:\t" + thread

    links = list(set(links)) # removing the duplicates
    print "files:\t" + str(len(links))

    current_files = os.listdir('.')
    
    for i in links:
        # some sites are using relative adressing
        link = i.replace("..", protocol + "://" + chan + "/" + board)
        if link.startswith("//"):
            link = protocol + ":" + link # fix for 4chan
        print "direct link:\t" + link

        filename = i.split("/")[-1]

        if filename in current_files:
            print "file already exists:\t" + filename
        else:
            wget.download(link)
            print "" # without it output is messed up

if __name__ == '__main__':
    main()
