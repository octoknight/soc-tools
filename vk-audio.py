import vk_api
import sys
#import wget # for downloading audio

def main():
	LOGIN = sys.argv[1]
	PASSWORD = sys.argv[2]
	USER_ID = sys.argv[3]
	vk = vk_api.VkApi(LOGIN, PASSWORD)

	try:
		vk.authorization()  
	except vk_api.AuthorizationError as error_msg:
		print(error_msg)
		return

	vals = {
		'owner_id': USER_ID,
		'count': 25,
		'need_user': 0
	}

	res = vk.method('audio.get', vals)
	for i in res['items']:
		owner_id = i['owner_id']
		title = i['title']
		artist = i['artist']
		audio_id = i['id']

		print audio_id, artist.encode('utf-8'), "-", title.encode('utf-8')

		newvals = {
			'owner_id': owner_id,
			'audio_id': audio_id,
			'title': title.lower(),
			'artist': artist.lower(),
			'no_search': 1         
		}
		vk.method('audio.edit', newvals)

if __name__ == '__main__':
	main()
