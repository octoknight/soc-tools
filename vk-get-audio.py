import vk_api
import sys
import time
import os
import wget # for downloading audio

def main():
	LOGIN = sys.argv[1]
	PASSWORD = sys.argv[2]
	USER_ID = sys.argv[3]
	vk = vk_api.VkApi(LOGIN, PASSWORD)

	try:
		vk.authorization()
	except vk_api.AuthorizationError as error_msg:
		print(error_msg)
		return

	folder = 'audio' + time.strftime("%H%M%S%d%m%Y")
        if not os.path.exists(folder):
                os.mkdir(folder)
        os.chdir(folder)

	vals = {
		'owner_id': USER_ID,
		'need_user': 0
	}
	res = vk.method('audio.get', vals)
	
	for i in res['items']:
		title = u(i['title'])
		artist = u(i['artist'])
		url = i['url']
		print '\n' + artist.encode('utf-8'), "-", title.encode('utf-8')
		wget.download(url)

if __name__ == '__main__':
	main()
